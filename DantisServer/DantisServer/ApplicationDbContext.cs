﻿using DantisServer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace DantisServer
{
    class ApplicationDbContext : DbContext
    {
        DbSet<Patient> Patiants { get; set; }
        DbSet<Visit> Visits { get; set; }
        DbSet<Organization> Organizations { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(@"C:\Users\HUAWEI\source\GitRepository\DantisServer\DantisServer\DantisServer")
                .AddJsonFile("appsettings.json")
                .Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }
    }    
}


