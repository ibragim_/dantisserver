﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DantisServer.Interfaces
{
    interface IEntity<T>
    {
        public T ID { get; set; }
    }
}
