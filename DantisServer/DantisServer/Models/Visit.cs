﻿using DantisServer.Enums;
using DantisServer.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DantisServer.Models
{
    public class Visit : IEntity<int>
    {

        /// <summary>
        /// Visitor ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Date of the patient's visit
        /// </summary>
        public DateTime VisitDate { get; set; }

        public VisitStatus VisitStatus { get; set; }
        public VisitType VisitType { get; set; }
   
        public decimal? Price { get; set; }

        /// <summary>
        /// FK_Visit_To_Patient ForeignKey (PatientId) References Patient(ID)
        /// </summary>
        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        public Patient Patient { get; set; }

    }
}
