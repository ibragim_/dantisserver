﻿using DantisServer.Enums;
using DantisServer.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DantisServer.Models
{
    public  class Patient : IEntity<int>
    {
        /// <summary>
        /// Patient ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Patient Name
        /// </summary>
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        /// <summary>
        /// Patient's date of brith
        /// </summary>
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        public Gender Gender { get; set; }
        public DateTime CreatedAt { get; set; }
        /// <summary>
        /// FK_Patient_To_Organization ForeignKey (OrganizationId) References Organization(ID)
        /// </summary>
        [ForeignKey("Organization")]
        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }

    }
}
