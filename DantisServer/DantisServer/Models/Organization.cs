﻿using DantisServer.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DantisServer.Models
{
    public class Organization : IEntity<int>
    {

        /// <summary>
        /// Organization ID
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Organization Name
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        /// <summary>
        /// Organization Address
        /// </summary>
        [Required]
        [StringLength(150)]
        public string Address { get; set; }
        /// <summary>
        /// Organization Supervisor
        /// </summary>
        [Required]
        [StringLength(100)]
        public string Supervisor { get; set; }
    }

}
